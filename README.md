NP02 Slow Control Application

v5.0.2

The web application that allows to monitor NP02 detector using data archived by NP02 DCS application.

Built using AngularJS, HTML, CSS, JavaScript, PHP, SQL Highcharts.

As is usual for AngularJS, each page is called a module and each module contains a component. Each page has it's own directory, which contains module, component and template files.

Images are stored in 'img' folder, whereas DB connectivity is executed using PHP scripts, located in 'php-db-conn' directory.

The charts are displayed using Highcharts and Highstock JavaScript plugins.

The camera streams are displayed using Paella Player. Important! Before updating Paella JS to newer version, make sure to save paella_player_es201.js, as it was modified to allow for better customization.