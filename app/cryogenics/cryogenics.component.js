'use strict';
angular.module('cryogenics', []).component('cryogenics', {
    templateUrl: 'cryogenics/cryogenics.template.html',
    controller: function cryogenicsController($scope, $http, $interval) {
        this.pageTitle = "NP02 cryogenics";
        this.natalie = 1;
        var self = this;

        this.reload = function () {

            $http.get("php-db-conn/cachedVals.conn.php?elemId=np02cryo").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(res[i]);
                }

                self.NP02_MHT0100AI = rArr[0];
                self.NP02_TT0100AI = rArr[1];
                self.NP02_PT0106AI = rArr[2];

                self.timestamp = rArr[rArr.length-1] * 1000;

            });

            $http.get("php-db-conn/cachedVals.conn.php?elemId=cryogenics").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(JSON.parse(res[i]));
                }


                /*self.NP02_MHT0100AI = rArr[0];
                self.NP02_TT0100AI = rArr[1];
                self.NP02_PT0106AI = rArr[2];*/

                self.NP02_DCS_01_2PT4910 = rArr[0];
                self.NP02_DCS_01_2QT4711 = rArr[1];
                self.NP02_DCS_01_2PDT4915 = rArr[2];
                self.NP02_DCS_01_2QT4710 = rArr[3];
                self.NP02_DCS_01_2QT4720 = rArr[4];
                self.NP02_DCS_01_2QT4730 = rArr[5];
                self.NP02_DCS_01_2CV4202 = rArr[6];
                self.NP02_DCS_01_2DPT4500 = rArr[7];
                self.NP02_DCS_01_2PT4432 = rArr[8];
                self.NP02_DCS_01_mp_DipSubFloat = rArr[9];

            });

            console.log("interval occured");

        };

        this.promise;

        this.reload();

        $scope.start = function() {
            $scope.stop();

            self.promise = $interval(self.reload, 60000);
        };

        $scope.stop = function() {
            $interval.cancel(self.promise);
        };
        $scope.start();

        $scope.$on('$destroy', function() {
            $scope.stop();
        });
    }
});