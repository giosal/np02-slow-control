'use strict';
angular.module('inside', []).component('inside', {
    templateUrl: 'inside/inside.template.html',
    controller: function insideController($scope, $http, $interval) {
        this.pageTitle = "NP02 T-Insulation";
        this.natalie = 1;
        this.TT0101 = "";
        var self = this;

        this.reload = function () {

            $http.get("php-db-conn/cachedVals.conn.php?elemId=np02cryo").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(res[i]);
                }

                self.NP02_MHT0100AI = rArr[0];
                self.NP02_TT0100AI = rArr[1];
                self.NP02_PT0106AI = rArr[2];

                self.timestamp = rArr[rArr.length - 1] * 1000;

            });

            $http.get("php-db-conn/cachedVals.conn.php?elemId=inside").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(JSON.parse(res[i]));
                }


                /*self.NP02_MHT0100AI = rArr[0];
                self.NP02_TT0100AI = rArr[1];
                self.NP02_PT0106AI = rArr[2];*/

                self.NP02_DCS_01_TSS_001_AA_BA = rArr[0];
                self.NP02_DCS_01_TSS_001_AA_BB = rArr[1];
                self.NP02_DCS_01_TSS_002_AA_BA = rArr[2];
                self.NP02_DCS_01_TSS_002_AH_BH = rArr[3];
                self.NP02_DCS_01_TSS_002_AI_BI = rArr[4];
                self.NP02_DCS_01_TSS_001_AD_BD = rArr[5];
                self.NP02_DCS_01_TSS_002_AB_BB = rArr[6];
                self.NP02_DCS_01_TSS_002_AF_BF = rArr[7];
                self.NP02_DCS_01_TSS_002_AM_BM = rArr[8];
                self.NP02_DCS_01_TSS_002_AN_BN = rArr[9];
                self.NP02_DCS_01_TSS_002_AO_BO = rArr[10];
                self.NP02_DCS_01_TSS_001_AC_BC = rArr[11];
                self.NP02_DCS_01_TSS_002_AD_BD = rArr[12];
                self.NP02_DCS_01_TSS_002_AJ_BJ = rArr[13];
                self.NP02_DCS_01_TSS_002_AK_BK = rArr[14];
                self.NP02_DCS_01_TSS_002_AL_BL = rArr[15];
                self.NP02_DCS_01_TSS_001_AE_BE = rArr[16];
                self.NP02_DCS_01_TSS_002_AE_BE = rArr[17];
                self.NP02_DCS_01_TSS_002_AP_BP = rArr[18];
                self.NP02_DCS_01_TSS_002_AQ_BQ = rArr[19];
                self.NP02_DCS_01_TSS_002_AR_BR = rArr[20];

                self.NP02_DCS_01_TE0010 = rArr[21];
                self.NP02_DCS_01_TE0011 = rArr[22];
                self.NP02_DCS_01_TE0012 = rArr[23];
                self.NP02_DCS_01_TE0013 = rArr[24];
                self.NP02_DCS_01_TE0014 = rArr[25];
                self.NP02_DCS_01_TE0015 = rArr[26];
                self.NP02_DCS_01_TE0016 = rArr[27];
                self.NP02_DCS_01_TE0017 = rArr[28];
                self.NP02_DCS_01_TE0018 = rArr[29];

                console.log("interval occured");
            });
        };

        this.promise;

        this.reload();

        $scope.start = function() {
            $scope.stop();

            self.promise = $interval(self.reload, 60000);
        };

        $scope.stop = function() {
            $interval.cancel(self.promise);
        };
        $scope.start();

        $scope.$on('$destroy', function() {
            $scope.stop();
        });
    }
});