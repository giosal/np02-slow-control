'use strict';
angular.module('lausanne', []).component('lausanne', {
    templateUrl: 'lausanne/lausanne.template.html',
    controller: function lausanneController($scope, $http, $q, $interval) {
        this.pageTitle = "NP02 Lausanne side";
        this.natalie = 1;
        var self = this;

        var element = [];

        element.push('TC_LS_02_A');
        element.push('TC_LS_02_B');
        element.push('BC_LS_03_A');
        element.push('BC_LS_03_B');
        element.push('3B_LS_01');
        element.push('MEM_LS_03');
        element.push('MEM_LS_02');
        element.push('MC_LJS_02');
        element.push('MC_LJS_03_A');
        element.push('MC_LJS_03_B');

        var elementsend = JSON.stringify(element);

        this.reload = function () {

            $http.get("php-db-conn/cachedVals.conn.php?elemId=np02cryo").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(res[i]);
                }

                self.NP02_MHT0100AI = rArr[0];
                self.NP02_TT0100AI = rArr[1];
                self.NP02_PT0106AI = rArr[2];

                self.timestamp = rArr[rArr.length-1] * 1000;

            });

            $http.get("php-db-conn/cachedVals.conn.php?elemId=lausanne").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(JSON.parse(res[i]));
                }


                /*self.NP02_MHT0100AI = rArr[0];
                self.NP02_TT0100AI = rArr[1];
                self.NP02_PT0106AI = rArr[2];*/
                self.TC_LS_02_A = rArr[0];
                self.TC_LS_02_B = rArr[1];
                self.BC_LS_03_A = rArr[2];
                self.BC_LS_03_B = rArr[3];
                self.B3_LS_01 = rArr[4];
                self.MEM_LS_03 = rArr[5];
                self.MEM_LS_02 = rArr[6];
                self.MC_LJS_02 = rArr[7];
                self.MC_LJS_03_A = rArr[8];
                self.MC_LJS_03_B = rArr[9];

            });
        };

        this.promise;

        this.reload();

        $scope.start = function() {
            $scope.stop();

            self.promise = $interval(self.reload, 150000);
        };

        $scope.stop = function() {
            $interval.cancel(self.promise);
        };
        $scope.start();

        $scope.$on('$destroy', function() {
            $scope.stop();
        });
    }
});