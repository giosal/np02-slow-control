'use strict';
angular.module('jura', []).component('jura', {
    templateUrl: 'jura/jura.template.html',
    controller: function juraController($scope, $http, $q, $interval) {
        this.pageTitle = "NP02 Jura side";
        this.natalie = 1;
        var self = this;

        var element = [];

        element.push('TC_JS_02_A');
        element.push('TC_JS_02_B');
        element.push('BC_JS_03_A');
        element.push('BC_JS_03_B');
        element.push('3B_JS_01');
        element.push('DS_JS_01');

        var elementsend = JSON.stringify(element);

        this.reload = function () {

            $http.get("php-db-conn/cachedVals.conn.php?elemId=np02cryo").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(res[i]);
                }

                self.NP02_MHT0100AI = rArr[0];
                self.NP02_TT0100AI = rArr[1];
                self.NP02_PT0106AI = rArr[2];

                self.timestamp = rArr[rArr.length - 1] * 1000;

            });

            $http.get("php-db-conn/cachedVals.conn.php?elemId=jura").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(res[i]);
                }

                self.TC_JS_02_A = rArr[0];
                self.TC_JS_02_B = rArr[1];
                self.BC_JS_03_A = rArr[2];
                self.BC_JS_03_B = rArr[3];
                self.B3_JS_01 = rArr[4];
                self.DS_JS_01 = rArr[5];

            });
        };

        this.promise;

        this.reload();

        $scope.start = function() {
            $scope.stop();

            self.promise = $interval(self.reload, 150000);
        };

        $scope.stop = function() {
            $interval.cancel(self.promise);
        };
        $scope.start();

        $scope.$on('$destroy', function() {
            $scope.stop();
        });
    }
});