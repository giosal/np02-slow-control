'use strict';
angular.module('comissioning', []).component('comissioning', {
    templateUrl: 'comissioning/comissioning.template.html',
    controller: function comissioningController($scope, $http, $interval) {
        this.pageTitle = "NP02 comissioning";
        this.natalie = 1;
        var self = this;

        this.reload = function () {

            $http.get("php-db-conn/cachedVals.conn.php?elemId=np02cryo").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(res[i]);
                }

                self.NP02_MHT0100AI = rArr[0];
                self.NP02_TT0100AI = rArr[1];
                self.NP02_PT0106AI = rArr[2];

                self.timestamp = rArr[rArr.length-1] * 1000;

            });

            $http.get("php-db-conn/cachedVals.conn.php?elemId=comissioning").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(JSON.parse(res[i]));
                }

                self.NP02_DCS_01_Heinz_I = rArr[0];
                self.NP04_DCS_01_Heinz_V_Raw = rArr[1];
                self.NP02_DCS_01_LE0301FK = rArr[2];
                self.NP02_DCS_01_LE0302FK = rArr[3];
                self.NP02_DCS_01_2PT4910 = rArr[4];
                self.NP02_DCS_01_2QT4711 = rArr[5];
                self.NP02_DCS_01_2PDT4915 = rArr[6];
                self.NP02_DCS_01_2QT4710 = rArr[7];
                self.NP02_DCS_01_2QT4720 = rArr[8];
                self.NP02_DCS_01_2QT4730 = rArr[9];
                self.NP02_DCS_01_LE0101 = rArr[10];
                self.NP02_DCS_01_LE0102 = rArr[11];
                self.NP02_DCS_01_LE0103 = rArr[12];
                self.NP02_DCS_01_LE0104 = rArr[13];
                self.NP02_DCS_01_LE0201 = rArr[14];
                self.NP02_DCS_01_LE0202 = rArr[15];
                self.NP02_DCS_01_LE0203 = rArr[16];
                self.NP02_DCS_01_LE0204 = rArr[17];
                self.NP02_DCS_01_LE0301 = rArr[18];
                self.NP02_DCS_01_LE0302 = rArr[19];
                self.NP02_DCS_01_LE0303 = rArr[20];
                self.NP02_DCS_01_LE0304 = rArr[21];
                self.NP02_DCS_01_LE0401 = rArr[22];
                self.NP02_DCS_01_LE0402 = rArr[23];
                self.NP02_DCS_01_LE0403 = rArr[24];
                self.NP02_DCS_01_LE0404 = rArr[25];
                self.NP02_DCS_01_CRP_1_1_POS = rArr[26];
                self.NP02_DCS_01_CRP_2_1_POS = rArr[27];
                self.NP02_DCS_01_CRP_3_1_POS = rArr[28];
                self.NP02_DCS_01_CRP_4_1_POS = rArr[29];
                self.NP02_DCS_01_CRP_1_2_POS = rArr[30];
                self.NP02_DCS_01_CRP_1_3_POS = rArr[31];
                self.NP02_DCS_01_CRP_1_4_POS = rArr[32];
                self.NP02_DCS_01_CRP_2_2_POS = rArr[33];
                self.NP02_DCS_01_CRP_2_3_POS = rArr[34];
                self.NP02_DCS_01_CRP_2_4_POS = rArr[35];
                self.NP02_DCS_01_CRP_3_2_POS = rArr[36];
                self.NP02_DCS_01_CRP_3_3_POS = rArr[37];
                self.NP02_DCS_01_CRP_3_4_POS = rArr[38];
                self.NP02_DCS_01_CRP_4_2_POS = rArr[39];
                self.NP02_DCS_01_CRP_4_3_POS = rArr[40];
                self.NP02_DCS_01_CRP_4_4_POS = rArr[41];

            });

            console.log("interval occured");

        };

        this.promise;

        this.reload();

        $scope.start = function() {
            $scope.stop();

            self.promise = $interval(self.reload, 60000);
        };

        $scope.stop = function() {
            $interval.cancel(self.promise);
        };
        $scope.start();

        $scope.$on('$destroy', function() {
            $scope.stop();
        });
    }
});