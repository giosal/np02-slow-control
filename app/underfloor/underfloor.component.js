'use strict';
angular.module('underfloor', []).component('underfloor', {
    templateUrl: 'underfloor/underfloor.template.html',
    controller: function crptempsController($scope, $http, $window, $interval) {
        this.pageTitle = "NP02 underfloor temperatures";
        this.natalie = 1;
        var self = this;

        this.pageChooser = function (page) {
            $window.location.href = "#!/" + page;
        };

        this.reload = function () {

            $http.get("php-db-conn/cachedVals.conn.php?elemId=np02cryo").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(res[i]);
                }

                self.NP02_MHT0100AI = rArr[0];
                self.NP02_TT0100AI = rArr[1];
                self.NP02_PT0106AI = rArr[2];

                self.timestamp = rArr[rArr.length-1] * 1000;

            });

            $http.get("php-db-conn/cachedVals.conn.php?elemId=underfloor").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(JSON.parse(res[i]));
                }

                self.NP02_DCS_01_AIR_06_13 = rArr[0];
                self.NP02_DCS_01_AIR_06_14 = rArr[1];
                self.NP02_DCS_01_AIR_06_15 = rArr[2];
                self.NP02_DCS_01_AIR_06_16 = rArr[3];
                self.NP02_DCS_01_AIR_06_17 = rArr[4];
                self.NP02_DCS_01_AIR_06_18 = rArr[5];
                self.NP02_DCS_01_AIR_06_19 = rArr[6];
                self.NP02_DCS_01_AIR_06_20 = rArr[7];
                self.NP02_DCS_01_AIR_06_21 = rArr[8];
                /*self.NP02_MHT0100AI = rArr[0];
                self.NP02_TT0100AI = rArr[1];
                self.NP02_PT0106AI = rArr[2];*/
                console.log("interval occured");

            });
        };

        this.promise;

        this.reload();

        $scope.start = function() {
            $scope.stop();

            self.promise = $interval(self.reload, 150000);
        };

        $scope.stop = function() {
            $interval.cancel(self.promise);
        };
        $scope.start();

        $scope.$on('$destroy', function() {
            $scope.stop();
        });
    }
});