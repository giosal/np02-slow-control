'use strict';
angular.module('zmonitor', []).component('zmonitor', {
    templateUrl: 'zmonitor/zmonitor.template.html',
    controller: function zmonitorController($scope, $http, $q, $interval) {
        this.pageTitle = "NP02 Ground impedance monitor";
        this.natalie = 1;
        this.TT0101 = "";
        var self = this;

        this.reload = function () {

            $http.get("php-db-conn/cachedVals.conn.php?elemId=np02cryo").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(res[i]);
                }

                self.NP02_MHT0100AI = rArr[0];
                self.NP02_TT0100AI = rArr[1];
                self.NP02_PT0106AI = rArr[2];

                self.timestamp = rArr[rArr.length-1] * 1000;

            });

            $http.get("php-db-conn/cachedVals.conn.php?elemId=zmonitor").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(JSON.parse(res[i]));
                }

                self.NP02_DCS_01_gizmo_RES = rArr[0];
                self.NP02_DCS_01_gizmo_TH = rArr[1];
                self.NP02_DCS_01_gizmo_mag = rArr[2];
                self.NP02_DCS_01_gizmo_I = rArr[3];
                self.NP02_DCS_01_gizmo_Q = rArr[4];
                /*self.NP02_MHT0100AI = rArr[5];
                self.NP02_TT0100AI = rArr[6];
                self.NP02_PT0106AI = rArr[7];*/

                console.log("interval occured");

            });
        };

        this.promise;

        this.reload();

        $scope.start = function() {
            $scope.stop();

            self.promise = $interval(self.reload, 60000);
        };

        $scope.stop = function() {
            $interval.cancel(self.promise);
        };
        $scope.start();

        $scope.$on('$destroy', function() {
            $scope.stop();
        });
    }
});