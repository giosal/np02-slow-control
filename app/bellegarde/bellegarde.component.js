'use strict';
angular.module('bellegarde', []).component('bellegarde', {
    templateUrl: 'bellegarde/bellegarde.template.html',
    controller: function bellegardeController($scope, $http, $q, $interval) {
        this.pageTitle = "NP02 Bellegarde side";
        this.natalie = 1;
        var self = this;

        var element = [];

        element.push('MC_BSS_HOR_4B');
        element.push('MC_BSS_HOR_3B');
        element.push('MC_BSS_HOR_2B');
        element.push('4M_BS_VER_05');
        element.push('4M_BS_VER_04');
        element.push('4M_BS_VER_03');
        element.push('3B_BS_HOR_01');
        element.push('3B_BS_HOR_02');
        element.push('3B_BS_HOR_03');
        element.push('4B_BS_VER_02');
        element.push('2B_BS_HOR_01');
        element.push('4M_BS_VER_01');
        element.push('MC_BJS_02');
        element.push('MC_BJS_03_A');
        element.push('MC_BJS_03_B');

        var elementsend = JSON.stringify(element);

        this.reload = function () {

            $http.get("php-db-conn/cachedVals.conn.php?elemId=np02cryo").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(res[i]);
                }

                self.NP02_MHT0100AI = rArr[0];
                self.NP02_TT0100AI = rArr[1];
                self.NP02_PT0106AI = rArr[2];

                self.timestamp = rArr[rArr.length-1] * 1000;

            });

            $http.get("php-db-conn/cachedVals.conn.php?elemId=bellegarde").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(res[i]);
                }

                self.MC_BJS_02 = rArr[0];
                self.MC_BJS_03_A = rArr[1];
                self.MC_BJD_03_B = rArr[2];
                self.MC_BSS_HOR_4B = rArr[3];
                self.MC_BSS_HOR_3B = rArr[4];
                self.MC_BSS_HOR_2B = rArr[5];
                self.M4_BS_VER_05 = rArr[6];
                self.M4_BS_VER_04 = rArr[7];
                self.M4_BS_VER_03 = rArr[8];
                self.B3_BS_HOR_01 = rArr[9];
                self.B3_BS_HOR_02 = rArr[10];
                self.B3_BS_HOR_03 = rArr[11];
                self.B4_BS_VER_02 = rArr[12];
                self.B2_BS_HOR_01 = rArr[13];
                self.M4_BS_VER_01 = rArr[14];



            });
        };

        this.promise;

        this.reload();

        $scope.start = function() {
            $scope.stop();

            self.promise = $interval(self.reload, 150000);
        };

        $scope.stop = function() {
            $interval.cancel(self.promise);
        };
        $scope.start();

        $scope.$on('$destroy', function() {
            $scope.stop();
        });
    }
});