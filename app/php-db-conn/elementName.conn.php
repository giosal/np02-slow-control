<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");


    set_time_limit(0);
    ini_set('memory_limit', '6G');




$elemId = $_GET["elemId"];

//include("../config/".$elemId.".conf");

$db = '(DESCRIPTION=
      	(ADDRESS= (PROTOCOL=TCP) (HOST=pdbr1-s.cern.ch) (PORT=10121) )
      	(LOAD_BALANCE=on)
      	(ENABLE=BROKEN)
      	(CONNECT_DATA=
      		(SERVER=DEDICATED)
      		(SERVICE_NAME=PDBR.cern.ch)
      	)
      )';

$conn = oci_connect("np02dbro", "5Pontekorvo50!", $db);

if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

$elems = array();

if ($file = fopen("../config/".$elemId.".conf", "r")) {
    $i=0;
    while (($line = fgets($file)) !== false) {
        $data = explode(",", $line);
        $elems[$i] = array(
            0 => $data[0],
            1 => $data[1]
        );
        $i++;
       //var_dump($locks);

    }
    fclose($file);
} else {
    echo "Error while opening file";
}

//echo(json_encode($elems));


$outp = array();
$rr = array();
$arrlen = count($elems);

//$create = oci_parse($conn, "CREATE GLOBAL TEMPORARY TABLE TEMP_ELEMS (ELEMENT_ID NUMBER NOT NULL) ON COMMIT DELETE ROWS");
//oci_execute($create);

$insert = oci_parse($conn, "insert into TEMP_ELEMS (ELEMENT_ID) values (:elId)");
//$select = oci_parse($conn, "select TS, VALUE_NUMBER from (select TS, VALUE_NUMBER from NP02_DCS_01.VEVENTSCREEN where ELEMENT_ID = :elId order by TS desc) where rownum=1");

for ($i = 0; $i < $arrlen; $i++) {
    $el = $elems[$i][0];
    oci_bind_by_name($insert, ':elId', $el);
//    oci_bind_by_name($select, ':elId', $el);
    oci_execute($insert, OCI_DEFAULT);
//    oci_execute($select);
}

$stid = oci_parse($conn, "with C as (select b.ELEMENT_ID, max(b.TS) ts from NP02_DCS_01.VEVENTSCREEN b join (select ELEMENT_ID from TEMP_ELEMS order by ELEMENT_ID) A on (A.ELEMENT_ID = b.ELEMENT_ID) group by b.ELEMENT_ID) select d.VALUE_NUMBER, d.TS from NP02_DCS_01.VEVENTSCREEN d join C on (C.ELEMENT_ID=d.ELEMENT_ID and C.ts=d.TS)");
oci_execute($stid);

for ($i = 0; $i < $arrlen; $i++) {
    while($rs = oci_fetch_array($stid, OCI_ASSOC)) {
        $outp[] = $rs["VALUE_NUMBER"];
    }
        //$outp = '{"records":{'.$outp[$i].'}}';

}



oci_commit($conn);



oci_close($conn);

echo(json_encode($outp));
?>