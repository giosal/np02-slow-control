<?php

    set_time_limit(0);
    ini_set('memory_limit', '6G');
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);

    $elemId = $_GET["elemId"];

    $week = date( strtotime('-7 days') );
    //echo $week;
    $data = array();
    $temp = array();

    for ($i = $week; $i < date(strtotime('-1 days')); $i+=86400) {
        $json = json_decode(file_get_contents('http://epdtdi-vm-01.cern.ch:5000/day/'. date('Y-m-d', $i) .'/'.$elemId));
        for ($j = 0; $j < count($json); $j++) {
            $temp = array_merge($temp, $json[$j]);
        }
        $data = array_merge($data, $temp);
    }

    sort($data);

    //print_r($data);

    echo(json_encode($data));

?>