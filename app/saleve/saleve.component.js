'use strict';
angular.module('saleve', []).component('saleve', {
    templateUrl: 'saleve/saleve.template.html',
    controller: function saleveController($scope, $http, $q, $interval) {
        this.pageTitle = "NP02 Saleve";
        this.natalie = 1;
        var self = this;

        this.reload = function () {

            $http.get("php-db-conn/cachedVals.conn.php?elemId=np02cryo").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(res[i]);
                }

                self.NP02_MHT0100AI = rArr[0];
                self.NP02_TT0100AI = rArr[1];
                self.NP02_PT0106AI = rArr[2];

                self.timestamp = rArr[rArr.length-1] * 1000;

            });

            $http.get("php-db-conn/cachedVals.conn.php?elemId=saleve").then(function (resultArr) {
                var rArr = [];
                var resjson = angular.toJson(resultArr.data);
                var res = JSON.parse(resjson);
                for (var i = 0; i < res.length; i++) {
                    rArr.push(JSON.parse(res[i]));
                }


                /*self.NP02_MHT0100AI = rArr[0];
                self.NP02_TT0100AI = rArr[1];
                self.NP02_PT0106AI = rArr[2];*/
                self.M3_SS_VER_04 = rArr[0];
                self.M4_SS_VER_04 = rArr[1];
                self.M3_SS_VER_03 = rArr[2];
                self.M4_SS_VER_03 = rArr[3];
                self.B4_SS_HOR_01 = rArr[4];
                self.DS_SS_01 = rArr[5];
                self.B3_SS_HOR_01 = rArr[6];
                self.B3_SS_HOR_02 = rArr[7];
                self.B3_SS_HOR_03 = rArr[8];
                self.MEM1_SS_VER_01 = rArr[9];
                self.MEM1_SS_HOR_02 = rArr[10];
                self.FRM_SS_VER_02 = rArr[11];
                self.MEM1_SS_HOR_04 = rArr[12];
                self.MEM1_SS_3AX_05_HOR = rArr[13];
                self.MEM1_SS_VER_03 = rArr[14];
                self.M3_SS_VER_02 = rArr[15];
                self.FRM_SS_HOR_01 = rArr[16];
                self.FRM_SS_HOR_03 = rArr[17];
                self.M4_SS_VER_02 = rArr[18];
                self.B2_SS_HOR_01 = rArr[19];
                self.M3_SS_VER_01 = rArr[20];
                self.M4_SS_VER_01 = rArr[21];
                self.TT_CONTACT_BOTTOM = rArr[22];
                self.TT_AMBIANCE_BORROM = rArr[23];
                self.MC_SLS_HOR_4B = rArr[24];
                self.MC_SLS_HOR_3B = rArr[25];
                self.MC_SLS_HOR_2B = rArr[26];
                self.M3_SS_VER_05 = rArr[27];
                self.M4_SS_VER_05 = rArr[28];
                self.MEM1_SS_3AX_05_VER = rArr[29];
                self.MEM1_SS_3AX_05_45 = rArr[30];
                self.TT_CONTACT_TOP = rArr[31];
                self.TT_AMBIENCE_TOP = rArr[32];

            });
        };

        this.promise;

        this.reload();

        $scope.start = function() {
            $scope.stop();

            self.promise = $interval(self.reload, 150000);
        };

        $scope.stop = function() {
            $interval.cancel(self.promise);
        };
        $scope.start();

        $scope.$on('$destroy', function() {
            $scope.stop();
        });
    }
});